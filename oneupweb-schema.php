<?php
/*
Plugin Name: Oneupweb Schema Control
Plugin URI: https://www.oneupweb.com/services/website-development
Description: A plugin to add schema to your content
Author: Paul Houser
Author URI: https://www.oneupweb.com/about-us
Text Domain: oneupweb
Version: 1.0
*/

namespace Oneupweb;

class Schema
{
    protected static $instance = null;

    protected function __construct() {}
    protected function __clone() {}

    public static function instance()
    {
        if (!isset(static::$instance))
            static::$instance = new static;            
        
        return static::$instance;
    }

    public $blocks = [], $package, $plugin_dir, $local_vars,
           $libraries = [], $enabled = false;

    public static function setup()
    {
        
        add_action('admin_enqueue_scripts', [self::instance(), 'enqueue_scripts']);

        add_action('wp_head', [self::instance(), 'head_matter']);
        add_action('add_meta_boxes', [self::instance(), 'add_meta_box']);
        add_action('save_post', [self::instance(), 'save_meta_data']);

        add_action('Oneupweb/Schema/output_array', [self::instance(), 'override_data'], 9, 2);

    }

    public static function head_matter()
    {
        $self = self::instance();

        $object = get_queried_object();
        if (is_archive()) 
        {
            return; //not supported yet
        } 
        else if (is_page()) 
        {
            return; //not supported yet;
        } 
        else if (is_single()) 
        {

            $schema = apply_filters("Oneupweb/Schema/single", [
                "@context"          => "http://schema.org",
                "@type"             => "BlogPosting",
                "headline"          => $object->post_title,
                "datePublished"     => get_the_time('c', $object),
                "dateModified"      => get_the_time('c', $object),
                "description"       => $object->post_excerpt,
                "mainEntityOfPage"  => get_permalink($object),                                
            ], $object);

            $author = get_user_by('id', $object->post_author);

            if ($author) 
            {
                $schema["author"] = apply_filters("Oneupweb/Schema/author", [
                    "@type" => "Person",
                    "name"  => $author->display_name,
                ], $author);
            }

            $thumbnail = get_post_thumbnail_id($object);
            $feature = wp_get_attachment_image_src($thumbnail, 'full');
        } 
        else 
        {
            return;
        }

        $organization = apply_filters('Oneupweb/Schema/organization_name', '');
        $logo = apply_filters('Oneupweb/Schema/organization_logo', '');

        if (! empty($organization))
        {
            $schema["publisher"] = [
                "@type" => "Organization",
                "name"  => $organization,                
            ];

            if (! empty($logo)) 
            {
                $schema["publisher"]["logo"] = [
                    "@type"     => "ImageObject",
                    "url"       => $logo
                ];
            }
        }
        
        if (! empty($feature))
        {
            $schema["image"] = [
                "@type"   => "ImageObject",
                "url"     => $feature[0],
                "height"  => $feature[1],
                "width"   => $feature[2]
            ];
        }

        $schema = apply_filters("Oneupweb/Schema/output_array", $schema, $object);

        printf('<script type="application/ld+json">%s</script>', json_encode($schema));

    }

    public static function enqueue_scripts()
    {
        wp_enqueue_style('oneupweb-schema', plugins_url('oneupweb-schema.css', __FILE__), array(), null);
    }

    public static function add_meta_box()
    {
        $screens = apply_filters('Oneupweb/Schema/post_types', ['post']);

        foreach ($screens as $screen) 
        {
            add_meta_box(
                'oneupweb_schema',
                'Schema Data',  
                [self::instance(), 'meta_box_contents'],
                $screen
            );
        }
    }

    public static function schema_fields($post_id = null)
    {
        if (is_object($post_id))
            $post = $post_id;
        else if (is_numeric($post_id))
            $post = get_post($post_id);
        else if (is_null($post_id))
            return [];

        return apply_filters('Oneupweb/Schema/fields', [
            '@type' => [
                'key' => 'type',
                'label' => 'Type', 
                'placeholder' => 'BlogPosting'
            ],
            'headline' => [
                'label' => 'Headline', 
                'placeholder' => $post->post_title,
            ],
            'description' => [
                'key' => 'desc',
                'label' => 'Description',
                'placeholder' => $post->post_excerpt
            ],
        ], $post);
    }

    public static function meta_box_contents($post)
    {
        $values = get_post_custom($post->ID); ?>

        <p>These settings will take priority over any automatically determined Schema values</p>

        <?php foreach(self::schema_fields($post) as $key => $args): ?>

        <?php if (! empty($args['key'])) $key = $args['key'] ?>

        <?php $type = $args['type'] ?? 'text' ?>

        <div class="components-panel__row">
            <label for="oneupweb_schema_<?=$key?>"><?=$args['label']?></label>
            <input type="<?=$type?>" placeholder="<?=esc_attr(apply_filters("Oneupweb/Schema/$key/placeholder", $args['placeholder'], $post))?>"
                id="oneupweb_schema_<?=$key?>" name="schema_<?=$key?>" size="20" class="components-text-control__input"
                value="<?=$values["_schema_$key"][0] ?? ''?>">

            <?php if (isset($args['after'])): ?>
            <span class="identifier"><?=$args['after']?></span>
            <?php endif; ?>
        </div>

        <?php endforeach;

    }

    public static function save_meta_data($post_id)
    {
        if (defined('DOING_AUTOSAVE'))
            return;

        $schema_fields = self::schema_fields($post_id);

        foreach($schema_fields as $key => $args)
        {
            if (! empty($args['key'])) 
                $key = $args['key'];

            $value = filter_input(INPUT_POST, "schema_$key", FILTER_SANITIZE_STRING);
            update_post_meta($post_id, "_schema_$key", $value);
        }
    }

    public static function override_data($schema, $object)
    {
        if (isset($object->ID)) 
        {
            $schema_fields = self::schema_fields($object->ID);        
            $post_meta = get_post_custom($object->ID);
        } 
        else 
        {            
            return $schema;
        }

        foreach($schema_fields as $key => $args)
        {
            $meta_key = ! empty($args['key']) ? "_schema_{$args['key']}" : "_schema_$key";  
            
            if (empty($post_meta[$meta_key]) || empty($post_meta[$meta_key][0]))
                continue;

            $schema[$key] = $post_meta[$meta_key][0];
        }

        return $schema;
    }

}

Schema::setup();